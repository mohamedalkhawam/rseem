export const techArr: string[] = ["FRONT-END", "BACK-END", "MOBILE"];

export const techObj: { [key: string]: any } = {
  "FRONT-END": [
    // {name:'Html',icon:'/techImgs/html-5.svg'},
    // {name:'Css',icon:'/techImgs/css3.svg'},
    { name: "JavaScript", icon: "/techImgs/javascript.svg" },
    { name: "NextJs", icon: "/techImgs/Nextjs-logo.svg" },
    { name: "ReactJs", icon: "/techImgs/react-native.svg" },
    { name: "Angular", icon: "/techImgs/angularjs.svg" },
    { name: "VueJs", icon: "/techImgs/vuejs.svg" },
    { name: "Bootstrap", icon: "/techImgs/bootstrap.svg" },
    { name: "Tailwind", icon: "/techImgs/tailwind.svg" },
  ],
  "BACK-END": [
    { name: "Java", icon: "/techImgs/java.svg" },
    { name: "NET", icon: "/techImgs/c-sharp.svg" },
    { name: "PHP", icon: "/techImgs/php.svg" },
    { name: "Python", icon: "/techImgs/python.svg" },
    { name: "NodeJs", icon: "/techImgs/nodejs.svg" },
    { name: "Scala", icon: "/techImgs/scala.svg" },
    { name: "GoLang", icon: "/techImgs/golang.svg" },
    // {name:'Ruby',icon:'/techImgs/ruby.svg'},
  ],
  MOBILE: [
    { name: "Android", icon: "/techImgs/android-os.svg" },
    { name: "iOS", icon: "/techImgs/apple-logo.svg" },
    { name: "Xamarin", icon: "/techImgs/xamarin.svg" },
    { name: "React Native", icon: "/techImgs/react-native.svg" },
    { name: "Ionic", icon: "/techImgs/ionic.svg" },
    { name: "Flutter", icon: "/techImgs/flutter.svg" },
  ],
};

export const services = [
  {
    title: "Websites",
    body: "Make your place on the Internet better with our host.",
    icon: "/custome-softwares.png",
  },
  {
    title: "Mobile Applications",
    body: "The team will start planning to create a sophisticated, a stable, an integrated and an attractive design application.",
    icon: "/websites.svg",
  },
  {
    title: "Hosting websiteds and booking servers",
    body: "Make your place on the Internet better with our host.",
    icon: "/mobile-applications.svg",
  },
  {
    title: "Graphic Design",
    body: "The team will start planning to create a sophisticated, a stable, an integrated and an attractive design application.",
    icon: "/e-commerce.svg",
  },
  {
    title: "E-Marketing",
    body: "Make your place on the Internet better with our host.",
    icon: "/graphic-design.svg",
  },
  {
    title: "Web Application Programing",
    body: "The team will start planning to create a sophisticated, ",
    icon: "/artifical-intelligence.svg",
  },
];

export const customers = [
  { img: "/customer1.png", link: "https://www.themwl.org/ar" },
  { img: "/customer2.png", link: "https://msajed.com/" },
  { img: "/customer3.png", link: "https://muqawil.org/ar/login" },
  { img: "/customer4.png", link: "https://sca.sa/" },
  { img: "/customer5.png", link: "http://kifapos.com/" },
  { img: "/customer6.png", link: "https://iesest.com/" },
];
