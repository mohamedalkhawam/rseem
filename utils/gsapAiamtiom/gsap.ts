import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { TextPlugin } from "gsap/dist/TextPlugin";
import { useEffect, useRef } from "react";
import { responsiveLang } from "../responsive";
import { useTranslation } from "react-i18next";

type refType = {
  current: any;
};

function isOdd(num: number) {
  if (num % 2 === 0) return true;
  return false;
}

gsap.registerPlugin(ScrollTrigger, TextPlugin);

export function useFadeY(ref: refType, y: string | number, start?: string) {
  const animation = useRef<any>();
  useEffect(() => {
    animation.current = gsap.fromTo(
      ref.current,
      { autoAlpha: 0, duration: 0.5, y: y },
      {
        duration: 0.5,
        autoAlpha: 1,
        y: "0",
        scrollTrigger: {
          id: "image",
          trigger: ref.current,
          start: start ? start : "center 80%",
          toggleActions: "play none none reverse",
        },
      }
    );
    return () => animation.current.kill();
  }, []);
}
export function useFadeX(ref: refType, x: string | number, start?: string) {
  const { i18n } = useTranslation();
  const animation = useRef<any>();
  useEffect(() => {
    animation.current = gsap.fromTo(
      ref.current,
      { autoAlpha: 0, duration: 0.5, x: responsiveLang(i18n.language, -x, x) },
      {
        duration: 0.5,
        autoAlpha: 1,
        x: "0",
        scrollTrigger: {
          id: "image",
          trigger: ref.current,
          start: start ? start : "center 80%",
          toggleActions: "play none none reverse",
          toggleClass: "active",
        },
      }
    );
    return () => animation.current.kill();
  }, [i18n.language]);
}
export function headerAnimation(
  ref1: refType,
  ref2: refType,
  ref3: refType,
  ref4: refType,
  ref5: refType,
  setOpen: any,
  setAtStart: any
) {
  const y = gsap.utils.selector(ref2);
  const timeline = gsap.timeline({
    scrollTrigger: {
      id: "image",
      trigger: ref1.current,
      start: () => window.innerHeight,
      toggleActions: "play none none reset",
      onLeaveBack: () => {
        setAtStart(true);
        setOpen(false);
      },
      onEnter: () => {
        setAtStart(false);
      },
    },
  });
  timeline
    .fromTo(
      ref1.current,
      { autoAlpha: 1, y: "-122" },
      { duration: 1, autoAlpha: 1, y: "0", ease: "bounce" }
    )
    .to(y("img"), { attr: { src: "/logo2.png" }, duration: 0 })
    .fromTo(
      ref2.current,
      { scale: 0 },
      { scaleX: 0.2, attr: { src: "/logo2.png" }, duration: 0.3, ease: "back" }
    )
    .to(ref2.current, { scaleY: 0.5, duration: 0.2, ease: "back" })
    .to(ref2.current, { scaleX: 0.7, duration: 0.2, ease: "back" })
    .to(ref2.current, { scaleY: 0.8, duration: 0.2, ease: "back" })
    .to(ref2.current, { scaleX: 1, duration: 0.2, ease: "back" })
    .to(ref2.current, { scaleY: 1, duration: 0.2, ease: "back" })
    .fromTo(ref3.current, { autoAlpha: 0 }, { duration: 1, autoAlpha: 1 }, 1)
    .fromTo(
      ref4.current,
      { scale: 0 },
      { scale: 1, autoAlpha: 1, zIndex: 100 },
      1
    )
    .fromTo(ref5.current, { autoAlpha: 0 }, { autoAlpha: 1 }, 1);
  return timeline;
}

export function topHeaderAnimation(
  ref1: refType,
  ref2: refType,
  ref3: refType,
  ref4: refType,
  ref5: refType,
  open: boolean,
  setAtStart: any
) {
  const q = gsap.utils.selector(ref5);
  const y = gsap.utils.selector(ref2);

  const timeline = gsap.timeline({
    scrollTrigger: {
      id: "image",
      start: () => "3px",
      toggleActions: "play none none reset",
      onLeaveBack: () => {
        setAtStart(true);
      },
      onEnter: () => {
        setAtStart(false);
      },
    },
  });
  timeline
    .fromTo(
      ref1.current,
      { backgroundColor: "transparent" },
      { backgroundColor: "white", duration: "0.1" },
      0
    )
    .to(y("img"), { attr: { src: "/logo2.png" }, duration: "0.1" })
    .to(ref5.current, { color: "#444", duration: 0 }, 0)
    .fromTo(q("p"), { color: "#444" }, { color: "#444", duration: 0.1 }, 0)
    .fromTo(
      ref3.current,
      { color: open ? "#444" : "white" },
      { color: open ? "#444" : "#444", duration: 0.1 },
      0
    );

  return timeline;
}

export function popGroupAnimate(ref: any, start?: string) {
  return gsap.fromTo(
    ref,
    { scale: 0 },
    {
      duration: 0.8,
      scale: 1,
      ease: "back",
      scrollTrigger: {
        id: "image",
        trigger: ref,
        start: start ? start : "center 80%",
        toggleActions: "play none none reverse",
      },
    }
  );
}

export function homeAnimate(lang: string, ref: any, start?: string) {
  const timeline = gsap.timeline();
  timeline
    .fromTo(
      ref[0].current,
      { rotateY: 180, autoAlpha: 0, scale: 0 },
      { rotateY: 0, autoAlpha: 1, scale: 1, duration: 1.5 }
    )
    .fromTo(ref[1].current, { autoAlpha: 0 }, { autoAlpha: 1 })
    .fromTo(
      ref[2].current,
      { autoAlpha: 0, text: responsiveLang(lang, "نحو الإلهام", "K") },
      {
        autoAlpha: 1,
        text: responsiveLang(lang, "نحو الإلهام", "KEEP ON INSPIRING"),
      }
    )
    .fromTo(ref[3].current, { autoAlpha: 0 }, { autoAlpha: 1 })
    .fromTo(
      ref[4].current,
      { autoAlpha: 0, scale: 0 },
      { autoAlpha: 1, scale: 1 }
    )
    .fromTo(
      ref[5].current,
      { autoAlpha: 0, x: responsiveLang(lang, "-40", "40"), z: -1 },
      { autoAlpha: 1, x: 0 }
    )
    .fromTo(
      ref[6].current,
      { autoAlpha: 0, scaleX: 0 },
      { autoAlpha: 1, scaleX: 1, duration: 1 }
    )
    .fromTo(
      ref[7].current,
      { autoAlpha: 0, x: responsiveLang(lang, "-40", "40"), z: -1 },
      { autoAlpha: 1, x: 0 }
    );
  return timeline;
}

export function tabsAnimation(ref: refType, start?: string) {
  return gsap.fromTo(
    ref.current,
    { scale: 0 },
    {
      duration: 1,
      scale: 1,
      scrollTrigger: {
        id: "image",
        trigger: ref.current,
        start: start ? start : "center 80%",
        toggleActions: "play none none reverse",
      },
    }
  );
}
export function productsBox(ref: refType, ref2: refType, start?: string) {
  return gsap.fromTo(
    ref.current,
    { y: 400 },
    {
      duration: 1,
      y: 0,
      scrollTrigger: {
        id: "image",
        trigger: ref2.current,
        start: start ? start : "center 90%",
        toggleActions: "play none none reverse",
      },
    }
  );
}

export function buttonAnimate(
  lang: string,
  ref1: refType,
  ref2: refType,
  start?: string
) {
  const timeline = gsap.timeline({
    scrollTrigger: {
      id: "image",
      trigger: ref2.current,
      start: start ? start : "center 80%",
      toggleActions: "play none none reverse",
    },
  });
  timeline.fromTo(
    ref1.current,
    { autoAlpha: 0 },
    { duration: 0.1, autoAlpha: 0 }
  );
  timeline.fromTo(
    ref2.current,
    { x: responsiveLang(lang, -150, 150), autoAlpha: 0 },
    { duration: 1, x: 0, autoAlpha: 1 }
  );
  timeline.fromTo(ref1.current, { scale: 0 }, { autoAlpha: 1, scale: 1 });
  return timeline;
}

export function customersAnimation(ref: any, i: number) {
  const timeline = gsap.timeline({
    scrollTrigger: {
      id: "image",
      trigger: ref,
      start: "center 80%",
      toggleActions: "play none none reverse",
    },
  });
  timeline.fromTo(ref, { scale: 0 }, { duration: 0.5, scale: 1 });
  timeline.fromTo(
    ref,
    { x: 0 },
    { duration: 0.5, x: isOdd(i) ? -10 : +10, ease: "bounce" }
  );
  timeline.to(ref, { duration: 0.5, x: isOdd(i) ? +10 : -10, ease: "bounce" });
  timeline.to(ref, { duration: 0.5, x: 0, ease: "bounce" });

  return timeline;
}

export function swiperAnimation(ref: refType, start?: string) {
  return gsap.fromTo(
    ref.current,
    { scale: 0 },
    {
      duration: 1,
      scale: 1,
      scrollTrigger: {
        id: "image",
        trigger: ref.current,
        start: start ? start : "center 80%",
        toggleActions: "play none none reverse",
      },
    }
  );
}
// export function swiperContentsAnimation(ref1:refType,ref2:refType,ref3:refType,ref4:refType,start?:string){
//     const timeline = gsap.timeline({scrollTrigger:{
//         id:'image',
//         trigger:ref2.current,
//         start:'center 80%',
//         toggleActions:'play reverse play reverse',
//         horizontal: true
//       }})
//       timeline.fromTo(ref2.current,{scale:0},{duration:3,scale:1})
//       timeline.fromTo(ref3.current,{scale:0},{duration:2,scale:1})
//       timeline.fromTo(ref4.current,{scale:0},{duration:1,scale:1})

// }

// export function swiperContentsAnimations(ref:refType,ref2:refType,start?:string){
//     gsap.fromTo(ref.current,{scale:0},{duration:1,scale:1,scrollTrigger:{
//         id:'image',
//         trigger:ref2.current,
//         start:start?start:'center 80%',
//         toggleActions:'play none none reset',
//         horizontal: true,
//         markers:true
//       }})

// }

export function SubscribeAnimation(
  ref1: refType,
  ref2: refType,
  ref3: refType,
  ref4: refType,
  ref5: refType
) {
  const timeline = gsap.timeline({
    scrollTrigger: {
      id: "image",
      trigger: ref1.current,
      start: "center 80%",
      toggleActions: "play none none reverse",
    },
  });
  timeline
    .fromTo(ref1.current, { scaleY: 0 }, { duration: 1, scaleY: 1 })
    .fromTo(ref2.current, { scale: 0 }, { scale: 1 })
    .fromTo(ref3.current, { autoAlpha: 0, y: -20 }, { autoAlpha: 1, y: 0 })
    .fromTo(ref4.current, { autoAlpha: 0, y: -20 }, { autoAlpha: 1, y: 0 })
    .fromTo(ref5.current, { scaleY: 0 }, { scaleY: 1 });

  return timeline;
}

export function useFooterColAnimation(ref1: refType, ref2: refType) {
  const timeline = useRef<any>();
  useEffect(() => {
    timeline.current = gsap.timeline({
      scrollTrigger: {
        id: "image",
        trigger: ref1.current,
        start: "center 90%",
        toggleActions: "play none none reverse",
      },
    });
    timeline.current
      .fromTo(ref1.current, { autoAlpha: 0, y: -20 }, { autoAlpha: 1, y: 0 })
      .fromTo(ref2.current, { autoAlpha: 0 }, { autoAlpha: 1 });

    return () => timeline.current.kill();
  }, []);
}

export function useInputRotateAnimation(
  ref: refType,
  x: number,
  rotationValue: number,
  start?: string
) {
  const { i18n } = useTranslation();
  const timeline = useRef<any>();
  useEffect(() => {
    timeline.current = gsap.timeline({
      scrollTrigger: {
        id: "image",
        trigger: ref.current,
        start: "center 80%",
        toggleActions: "play none none reverse",
      },
    });
    timeline.current
      .fromTo(
        ref.current,
        { autoAlpha: 0, x: responsiveLang(i18n.language, -x, x) },
        { autoAlpha: 1, x: 0, duration: 1 }
      )
      .fromTo(
        ref.current,
        {
          rotateZ: responsiveLang(i18n.language, -rotationValue, rotationValue),
        },
        { duration: 0.5, rotateZ: 0 }
      );

    return () => timeline.current.kill();
  }, [i18n.language]);
}
