module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "home-header": "url('/homeHeader.jpg')",
        logo: "url('/logoBG.png')",
        rLogo: "url('/rLogo2.png')",
        services: "url('/servicesBG.png')",
        swiper: "url('/swiperBG.jpg')",
        device: "url('/fullMachine.png')",
        topHeader: "url('/topImage.png')",
      },
      fontFamily: {
        test: ["Almarai"],
      },
      colors: {
        primary: {
          100: "#00B8D4",
          200: "#003D63",
          300: "#00253B",
        },
      },
    },
  },
  plugins: [],
};
