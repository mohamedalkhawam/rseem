type menuButtonProps = {
  active?: boolean;
  className?: string;
  onClick?: () => void;
};

const MenuButton = ({ active, className, onClick }: menuButtonProps) => {
  return (
    <div
      aria-label="Menu Buttons"
      className={`relative z-40 w-12 h-11 cursor-pointer flex flex-col justify-center items-center overflow-hidden bg-blue bg-opacity-40   ${className}`}
      onClick={onClick}
    >
      <span
        className={`absolute  h-[3px] bg-primary-100 top-3  transition-all transform duration-500 ${
          active ? "rotate-45  translate-y-[8.5px] w-6" : "w-6 left-[10px]"
        }`}
      />
      <span
        className={`absolute w-7 h-[3px] bg-primary-100 duration-500 transition-all transform ${
          active ? "translate-x-12 opacity-0" : ""
        } `}
      />
      <span
        className={`absolute h-[3px] bg-primary-100 bottom-3 transition-all transform duration-500 ${
          active
            ? "rotate-[135deg]  -translate-y-[8.5px] w-6"
            : "w-4 left-[10px]"
        }`}
      />
    </div>
  );
};

export default MenuButton;
