import Image from "next/image";
import { useEffect, useRef } from "react";
import { customers } from "../utils/consts";
import {
  buttonAnimate,
  customersAnimation,
  useFadeY,
} from "../utils/gsapAiamtiom/gsap";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../utils/responsive";
import Link from "next/link";
import { useRouter } from "next/router";
export default function TopCustomers() {
  const { t, i18n } = useTranslation();
  const router = useRouter();

  const titleRef = useRef(null);
  const bodyRef = useRef(null);
  const imgRef = useRef(null);
  const buttonRef = useRef(null);
  const circleButtonRef = useRef(null);
  const customersRef = useRef<HTMLElement[]>([]);
  const addToRef = (el: HTMLElement | null) => {
    if (el && !customersRef.current.includes(el)) {
      customersRef.current.push(el);
    }
  };
  useFadeY(titleRef, -10);
  useFadeY(bodyRef, -10);
  useFadeY(imgRef, 0);
  const animation = useRef<any>([]);
  const buttonAnimation = useRef<any>();
  useEffect(() => {
    const animations = animation.current;
    buttonAnimation.current = buttonAnimate(
      i18n.language,
      buttonRef,
      circleButtonRef
    );
    customersRef.current.map((el, i) => {
      animations.push(customersAnimation(el, i));
    });
    return () => {
      buttonAnimation.current.kill();
      animations.map((anim: any) => anim.kill());
    };
  }, []);

  return (
    <div className="px-[10%] mt-12" aria-label="Top Customers">
      <div className="flex flex-col lg:flex-row">
        <div className="flex flex-col justify-center  sm:w-[80%] mx-auto lg:w-1/3 order-last lg:order-first">
          <h3 ref={titleRef} className=" textSubtitle text-primary-100   mb-7">
            {t("top_customers")}
          </h3>
          <p ref={bodyRef} className="textBody leading-5 text-gray-500 ">
            {t("customers_info")}
          </p>
          <div className="group flex justify-end mt-12 ">
            <div
              ref={circleButtonRef}
              className={`relative  flex justify-center w-9 h-9 bg-primary-100 rounded-full p-1 z-[10] group-hover:bg-transparent  group-hover:border-2 group-hover:border-primary-100/60 ${responsiveLang(
                i18n.language,
                "ml-2",
                "mr-2"
              )} transition-[margin] duration-300 `}
            >
              <Image
                src="/rLogo2.png"
                alt="logo"
                width="30"
                height="18"
                className="transitition-all duration-500 opacity-0 group-hover:opacity-100 transform group-hover:rotate-[360deg] "
              />
            </div>
            {/* <div
              ref={circleButtonRef}
              className={`  bg-primary-100 rounded-full ${responsiveLang(
                i18n.language,
                "ml-6",
                "mr-3 sm:mr-6"
              )}  `}
            /> */}
            <div
              className={`transform transition-all h-full duration-[350ms] ${responsiveLang(
                i18n.language,
                "group-hover:-translate-x-3",
                "group-hover:translate-x-3"
              )}`}
            >
              <button
                ref={buttonRef}
                onClick={() => router.push("/requestService")}
                className={`bg-white h-full px-6 shrink-0 rounded-full font-myriad font-normal relative shadow-sm shadow-primary-100/40 border border-primary-100/20 `}
              >
                {t("customers_more")}
              </button>
            </div>
          </div>
        </div>
        <div
          ref={imgRef}
          className={`relative mx-auto ${responsiveLang(
            i18n.language,
            "lg:-left-20",
            "lg:-right-20"
          )}`}
        >
          <Image src="/laptop.png" alt="laptop" width="640" height="362" />
        </div>
      </div>
      <ul className="flex flex-wrap justify-center mt-10 mb-5">
        {customers.map(({ img, link }, i) => (
          <li ref={addToRef} key={i} className="">
            <a
              href="link"
              target="_blank"
              className=" relative cursor-pointer border shadow-lg mx-8 mb-6 flex justify-center items-center  rounded-2xl w-[250px] h-[180px]"
            >
              <Image
                src={img}
                alt="customer"
                width={i === 0 || i === 2 || i === 3 ? "180" : "120"}
                height={i === 0 || i === 2 || i === 3 ? "80" : "120"}
              />
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}
