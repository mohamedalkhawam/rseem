import { RiMailSendLine } from "react-icons/ri";
import { FiArrowRight } from "react-icons/fi";
import Image from "next/image";
import { useEffect, useRef } from "react";
import { SubscribeAnimation } from "../utils/gsapAiamtiom/gsap";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../utils/responsive";
import { FaRegEnvelope } from "react-icons/fa";
const SubscribeBox = () => {
  const { t, i18n } = useTranslation();
  const subscribeBoxRef = useRef(null);
  const subscribeImgRef = useRef(null);
  const subscribeTitleRef = useRef(null);
  const subscribeSubtitleRef = useRef(null);
  const subscribeButtonRef = useRef(null);

  useEffect(() => {
    SubscribeAnimation(
      subscribeBoxRef,
      subscribeImgRef,
      subscribeTitleRef,
      subscribeSubtitleRef,
      subscribeButtonRef
    );
  }, []);
  return (
    <div
      ref={subscribeBoxRef}
      className={`flex flex-col md:flex-row justify-center md:justify-start items-center  w-full h-48 ${responsiveLang(
        i18n.language,
        "subscribeBG-ar",
        "subscribeBG-en"
      )}  rounded-[50px] px-[5%]`}
      aria-label="subscribe to newsletter"
    >
      <div className="flex items-center ">
        <div
          ref={subscribeImgRef}
          className="relative shrink-0 w-[45px] h-[60px] md:w-[70px] md:h-[70px]"
        ></div>
        <div className="text-white ">
          <p
            ref={subscribeTitleRef}
            className="font-semibold md:text-xl text-lg lg:text-3xl mb-1 text-white md:mb-4"
          >
            {t("subscribe_title")}
          </p>
          <p
            ref={subscribeSubtitleRef}
            className=" text-base md:text-base lg:text-lg text-white mb-3 md:mb-0"
          >
            {t("subscribe_subtitle")}
          </p>
        </div>
      </div>
      <div
        ref={subscribeButtonRef}
        className={`h-12 md:h-16 w-10/12 min-w-[40%] max-w-[20rem] md:max-w-0 rounded-full bg-white flex items-center mx-auto ${responsiveLang(
          i18n.language,
          "md:ml-0 md:mr-auto pl-2",
          "md:mr-0 md:ml-auto pr-2"
        )}   `}
      >
        <input
          placeholder={t("write_here")}
          className="w-full h-full rounded-full outline-0 px-4"
        />
        <button className="group flex items-center  w-[4rem] md:w-[7.5rem] shrink-0 bg-[#FE4C1C] placeholder:text-xs rounded-full h-5/6 relative">
          <span
            className={`text-white text-md font-semibold  ${responsiveLang(
              i18n.language,
              "pr-3",
              "pl-3"
            )} md:opacity-0 transition-all delay-[50ms] group-hover:opacity-100`}
          >
            {t("send")}
          </span>
          <div
            className={`hidden md:flex items-center justify-center w-12 h-12 bg-white/40 rounded-full transition-all duration-300 absolute ${responsiveLang(
              i18n.language,
              "right-2 group-hover:right-16 transform rotate-180",
              "left-2 group-hover:left-16"
            )} left-2 group-hover:left-16 `}
          >
            {<FiArrowRight size={30} color="white" />}
          </div>
        </button>
      </div>
    </div>
  );
};

export default SubscribeBox;
