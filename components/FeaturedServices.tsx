import Service from "./Service";
import Image from "next/image";
import React, { useEffect, useRef } from "react";
import {
  useFadeX,
  useFadeY,
  popGroupAnimate,
} from "../utils/gsapAiamtiom/gsap";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../utils/responsive";
import { t } from "i18next";
import { useRouter } from "next/router";

export default function FeaturedServices() {
  const router = useRouter();
  const services = [
    {
      title: t("websites"),
      body: t("websites_info"),
      icon: "/websites.svg",
      link: `/services/websites`,
    },
    {
      title: t("mobile-applications"),
      body: t("mobile-applications_info"),
      icon: "/mobile-applications.svg",
      link: `/services/mobile-applications`,
    },
    {
      title: t("custome-softwares"),
      body: t("custome-softwares_info"),
      icon: "/custome-softwares.svg",
      link: `/services/custome-softwares`,
    },
    {
      title: t("e-commerce"),
      body: t("e-commerce_info"),
      icon: "/e-commerce.svg",
      link: `/services/e-commerce`,
    },
    {
      title: t("hosting"),
      body: t("hosting_info"),
      icon: "/hosting.svg",
      link: `/services/hosting`,
    },
    {
      title: t("startups"),
      body: t("startups_info"),
      icon: "/startups.svg",
      link: `/services/startups`,
    },
    {
      title: t("graphic-design"),
      body: t("graphic-design_info"),
      icon: "/graphic-design.svg",
      link: `/services/graphic-design`,
    },
    {
      title: t("digital-marketing"),
      body: t("digital-marketing_info"),
      icon: "/digital-marketing.svg",
      link: `/services/digital-marketing`,
    },
    {
      title: t("artifical-intelligence"),
      body: t("artifical-intelligence_info"),
      icon: "/artifical-intelligence.svg",
      link: `/services/artifical-intelligence`,
    },
    {
      title: t("virtual-reality"),
      body: t("virtual-reality_info"),
      icon: "/virtual-reality.svg",
      link: `/services/virtual-reality`,
    },
  ];

  const { i18n } = useTranslation();

  const titleRef = useRef(null);
  const bodyRef = useRef(null);
  const imgRef = useRef(null);
  const img2Ref = useRef(null);
  const requestRef = useRef(null);
  const servicesRef = useRef<HTMLElement[]>([]);
  const addToRef = (el: HTMLElement | null) => {
    if (el && !servicesRef.current.includes(el)) {
      servicesRef.current.push(el);
    }
  };
  useFadeY(titleRef, -40);
  useFadeY(requestRef, 40, "center 95%");
  useFadeY(bodyRef, -20);
  useFadeX(imgRef, 100);
  useFadeX(img2Ref, -100);
  const animation = useRef<any>([]);
  useEffect(() => {
    const animations = animation.current;
    servicesRef.current.map((el) => {
      animations.push(popGroupAnimate(el));
    });
    return () => animations.map((anim: any) => anim.kill());
  }, []);
  return (
    <div
      className=" flex flex-col items-center sm:block bg-services bg-cover bg-right relative px-[10%] pt-44 sm:pt-14 pb-24 sm:pb-20"
      aria-label="featured services"
    >
      <div
        ref={imgRef}
        className={` absolute ${responsiveLang(
          i18n.language,
          "sm:left-20",
          "sm:right-20"
        )}  -top-28 sm:-top-32 `}
      >
        <div>
          <Image
            src="/screenImage.png"
            alt="2 persons lookaing at screen"
            height="250"
            width="250"
          />
        </div>
        <div ref={img2Ref} className="absolute -top-0 left-0 rotate">
          <div className="counterrotate">
            <Image src="/rLogo.png" alt="R logo" height="100" width="100" />
          </div>
        </div>
      </div>
      <h3
        ref={titleRef}
        className={` font-medium text-white text-3xl md:text-4xl mb-8 text-center ${responsiveLang(
          i18n.language,
          " sm:text-right",
          " sm:text-left"
        )}`}
      >
        {t("featured_services")}
      </h3>
      <p
        ref={bodyRef}
        className={`textBody text-white sm:w-[60%] mb-16 sm:mb-24 text-center  ${responsiveLang(
          i18n.language,
          "sm:text-right",
          "sm:text-left"
        )} `}
      >
        {t("services_info")}
      </p>
      <ul className="flex flex-wrap justify-center px-[8%]">
        {services.slice(0, 8).map((service, i) => (
          <li
            ref={addToRef}
            key={i}
            onClick={() => router.push(service.link)}
            className={`cursor-pointer mb-6 ${
              i % 2 === 0
                ? responsiveLang(
                    i18n.language,
                    "sm:ml-[30%] xl:ml-[9%]",
                    "sm:mr-[30%] xl:mr-[9%]"
                  )
                : responsiveLang(
                    i18n.language,
                    "sm:mr-[30%] xl:mr-0",
                    "sm:ml-[30%] xl:ml-0"
                  )
            }`}
          >
            <Service
              title={service.title}
              body={service.body}
              icon={service.icon}
            />
          </li>
        ))}
      </ul>
      <div ref={requestRef} className="flex justify-center mt-12">
        <button
          onClick={() => router.push("/requestService")}
          className="text-white hover:text-primary-200  hover:bg-white/80  border-white px-12 py-3 rounded-md  text-lg font-semibold border "
        >
          {t("request-service")}
        </button>
      </div>
      {/*   <div ref={requestRef} className={`group mx-auto mt-5 relative overflow-hidden bg-gradient-to-r  from-primary-300 to-primary-100 w-48 -z-1 text-center py-2 rounded-lg cursor-pointer hover:drop-shadow-lg `}>
              <div className="bg-white absolute w-[105%]  rounded-md top-0 -left-1 fullRotation " />
            <div className='bg-gradient-to-r from-primary-300 to-primary-100 bg-clip-text text-transparent relative z-1 text-lg font-semibold select-none '>
              {t('request-service')}
            </div>
            </div> */}
    </div>
  );
}
