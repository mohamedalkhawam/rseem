import { t } from "i18next";
import Image from "next/image";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../utils/responsive";

type serviceProps = {
  title: string;
  body: string;
  icon: string;
};

const Service = ({ title, body, icon }: serviceProps) => {
  const { i18n } = useTranslation();
  return (
    <div
      aria-label="service"
      className={`flex flex-col items-center sm:items-start  sm:flex-row bg-white shadow-lg border rounded-xl sm:h-[10rem]  sm:w-[23rem] p-8  `}
    >
      <div
        className={`shrink-0 ${responsiveLang(
          i18n.language,
          "sm:ml-5",
          "sm:mr-5"
        )}  mb-4 sm:mb-0`}
      >
        <Image src={icon} alt={title} width="75" height="75" />
      </div>
      <div>
        <h3
          className={`font-myriad font-bold text-[#276992] text-xl leading-5 mb-3 text-center ${responsiveLang(
            i18n.language,
            "sm:text-right",
            "sm:text-left"
          )}`}
        >
          {title}
        </h3>
        <p
          className={` font-light leading-5 text-center text-gray-500 ${responsiveLang(
            i18n.language,
            "sm:text-right text-sm",
            "sm:text-left "
          )}`}
        >
          {body.slice(0, 70) + "..." + t("more")}
        </p>
      </div>
    </div>
  );
};

export default Service;
