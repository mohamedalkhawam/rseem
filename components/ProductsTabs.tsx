import { useEffect, useRef, useState } from "react";
import InPageTabs from "./InPageTabs";
import Image from "next/image";
import { useTranslation } from "react-i18next";
import { useFadeX, productsBox } from "../utils/gsapAiamtiom/gsap";
import gsap from "gsap";
import { responsiveLang } from "../utils/responsive";
import { t } from "i18next";

type itemProp = {
  id: number;
};

const products = [
  [
    { id: 1, img: "/portfolio1.jpeg" },
    { id: 2, img: "/portfolio2.jpeg" },
    { id: 3, img: "/portfolio3.jpeg" },
    { id: 4, img: "/portfolio4.jpeg" },
    { id: 5, img: "/portfolio5.jpeg" },
  ],
  [
    { id: 5, img: "/product.png" },
    { id: 6, img: "/product.png" },
    { id: 7, img: "/product.png" },
    { id: 8, img: "/product.png" },
    { id: 9, img: "/product.png" },
  ],
];

const ProductsTabs = () => {
  const { i18n } = useTranslation();
  const [id, setId] = useState(2);
  const [items, setItems] = useState<{}[]>([]);
  const preId = useRef(2);
  const tlRef: any = useRef({});
  const itemsRef = useRef<HTMLElement[][]>([[], [], [], []]);
  // console.log(itemsRef.current);
  // console.log(tlRef.current);

  const addToRef = (el: HTMLElement | null) => {
    if (el && !itemsRef.current[id].includes(el)) {
      itemsRef.current[id].push(el);
    }
  };

  function animateProducts() {
    if (!tlRef.current[id] && id !== 2) {
      tlRef.current[id] = gsap.timeline();
      items.map((item, i) => {
        tlRef.current[id].add(
          gsap.fromTo(
            itemsRef.current[id][i],
            { scale: 0, autoAlpha: 0 },
            { scale: 1, autoAlpha: 1, duration: 1, ease: "bounce" }
          ),
          0.2 * i
        );
      });
    }
  }

  useEffect(() => {
    animateProducts();
  }, [id]);

  useEffect(() => {
    preId.current !== 2 && tlRef.current[preId.current].progress(0).pause();
    id !== 2 && tlRef.current[id].restart();
    preId.current = id;
  }, [id]);

  const imgRef = useRef(null);
  const titleRef = useRef(null);
  const bodyRef = useRef(null);
  const productsBoxRef = useRef(null);
  const boxReference = useRef(null);
  useFadeX(imgRef, -200);
  useFadeX(titleRef, 200);
  useFadeX(bodyRef, 200);
  const animation = useRef<any>();
  useEffect(() => {
    animation.current = productsBox(productsBoxRef, boxReference);

    return () => animation.current.kill();
  }, []);

  return (
    <div aria-label="products Tab">
      <div className="flex flex-col items-center lg:flex-row  justify-center mb-8">
        <div
          ref={imgRef}
          className={`relative ${responsiveLang(
            i18n.language,
            "lg:ml-24",
            "lg:mr-24"
          )}  -top-12 shrink-0  w-[70vw] h-[70vw] max-w-[370px] max-h-[370px] `}
        >
          <Image
            src="/fullMachine.png"
            alt="machine"
            layout="fill"
            className="rounded-full"
          />
        </div>
        <div
          className={`text-center  ${responsiveLang(
            i18n.language,
            "lg:text-right",
            "lg:text-left"
          )}`}
        >
          <h3
            ref={titleRef}
            className="font-cake textSubtitle text-primary-100 lg:pt-12 pb-7"
          >
            {t("port_products")}
          </h3>
          <p ref={bodyRef} className="textBody text-gray-500 ">
            {t("products_info")}
          </p>
          <div className="flex  justify-center lg:justify-start mt-8">
            <InPageTabs
              getId={(i) => {
                setId(i);
                setItems(products[i]);
              }}
              options={[t("portfolio"), t("solutions")]}
            />
          </div>
        </div>
      </div>
      <div ref={boxReference} />
      <div ref={productsBoxRef} className=" w-full flex flex-col items-center">
        <div className="flex flex-wrap justify-center min-h-[70vh]">
          {items.map((item: any, i) => (
            <div
              ref={addToRef}
              key={i}
              className="p-4 border  shadow-md hover:shadow-lg  cursor-pointer    rounded-3xl h-[250px] w-[330px] sm:w-[300px] flex justify-center items-center  sm:mx-5  mb-7"
            >
              <Image src={item.img} alt="product" width="300" height="170" />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductsTabs;
