import Image from "next/image";
import { useEffect, useRef } from "react";
import { techArr, techObj } from "../utils/consts";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useFadeX, useFadeY } from "../utils/gsapAiamtiom/gsap";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../utils/responsive";
import { t } from "i18next";
gsap.registerPlugin(ScrollTrigger);

type itemType = any;

export default function Tecnologies() {
  const { i18n } = useTranslation();
  const imgRef = useRef(null);
  const titleRef = useRef(null);
  const bodyRef = useRef(null);
  const frontRef = useRef(null);
  const backRef = useRef(null);
  const MobileRef = useRef(null);
  useFadeY(imgRef, -30);
  useFadeY(titleRef, 0);
  useFadeY(bodyRef, 0);
  useFadeX(frontRef, -200);
  useFadeY(backRef, 100);
  useFadeX(MobileRef, 200);
  return (
    <>
      <div
        ref={imgRef}
        className="flex justify-center relative bottom-12 px-12 sm:px-0 "
        aria-label="technologies"
      >
        <Image src="/devices.png" alt="devices" width="400" height="320" />
      </div>
      <h3
        ref={titleRef}
        className="textSubtitle px-3  text-center text-primary-100 tracking-wide"
      >
        {t("techs_used")}
      </h3>
      <p
        ref={bodyRef}
        className="textBody text-center text-md px-3 max-w-[40rem] mx-auto mt-12"
      >
        {t("tech_info")}
      </p>
      <ul className="flex flex-wrap px-[5%] justify-around mt-20 ">
        {techArr.map((title, i) => (
          <li
            ref={i === 0 ? frontRef : i === 1 ? backRef : MobileRef}
            key={title + i * 2}
            className="flex flex-col items-center py-10 m-3  border rounded-3xl w-[15rem] min-h-[21rem] bg-primary-100/10"
          >
            <div dir="ltr" className="flex items-center  ">
              <div
                className={`w-[1.1rem] h-[1.1rem]  motion-safe:animate-pulse bg-primary-100 rounded-full ${responsiveLang(
                  i18n.language,
                  "ml-1",
                  "mr-1"
                )} mr-1 mt-1 `}
              />
              <h2 className="text-2xl  text-primary-200 font-cake md:font-semibold ">
                {title}
              </h2>
            </div>
            <ul dir="ltr" className="w-full mt-3 flex flex-col pl-16 gap-y-2 ">
              {techObj[title].map((ite: itemType, j: number) => (
                <li key={ite.name} className="flex items-center">
                  {ite?.icon && (
                    <Image
                      src={ite.icon}
                      alt={ite.icon}
                      width={30}
                      height={30}
                    />
                  )}
                  <p
                    className={`text-center text-gray-600 mx-3  capitalize ${
                      j == 0 && i == 1 ? "mt-1" : ""
                    }`}
                  >
                    {ite.name}
                  </p>
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </>
  );
}
