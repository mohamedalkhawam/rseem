import BaseInput from "./inputs/baseInput";
import { useEffect, useRef, useState } from "react";
import BaseTextarea from "./inputs/BaseTextarea";
import { useFadeY, useInputRotateAnimation } from "../utils/gsapAiamtiom/gsap";
import Option from "../components/option";
import { t } from "i18next";
import axios from "axios";
import { useRouter } from "next/router";

const ContactUs = ({ requestService }: any) => {
  const router = useRouter();
  const [services, setServices] = useState([
    {
      id: 1,
      title: "websites",
      icon: "/websites.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 2,
      title: "mobile-applications",
      icon: "/mobile-applications.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 3,
      title: "custome-softwares",
      icon: "/custome-softwares.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 4,
      title: "e-commerce",
      icon: "/e-commerce.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 5,
      title: "hosting",
      icon: "/hosting.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 6,
      title: "startups",
      icon: "/startups.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 7,
      title: "graphic-design",
      icon: "/graphic-design.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 8,
      title: "digital-marketing",
      icon: "/digital-marketing.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 9,
      title: "artifical-intelligence",
      icon: "/artifical-intelligence.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
    {
      id: 10,
      title: "virtual-reality",
      icon: "/virtual-reality.svg",
      iconAlt: "three floating screens",
      selected: false,
    },
  ]);

  const selectService = (id: number) => {
    setServices(
      services.map((item, i) =>
        i === id ? { ...item, selected: !services[id].selected } : item
      )
    );
  };
  const titleRef = useRef(null);
  const nameRef = useRef(null);
  const subRef = useRef(null);
  const emailRef = useRef(null);
  const mobileRef = useRef(null);
  const messageRef = useRef(null);
  const buttonRef = useRef(null);
  useInputRotateAnimation(nameRef, -200, 10);
  useInputRotateAnimation(subRef, 200, -10);
  useInputRotateAnimation(emailRef, -200, 10);
  useInputRotateAnimation(mobileRef, 200, -10);
  useFadeY(messageRef, 40);
  useFadeY(titleRef, 0);
  useFadeY(buttonRef, 0);

  const [formData, setFormData] = useState({
    name: "",
    subject: "",
    email: "",
    phone: "",
    message: "",
  });
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const handleChange = (e: any) => {
    let valid;
    switch (e.target.name) {
      case "phone":
        if (e.target.value.length === 0) {
          setPhoneErrorMessage("");
          break;
        }
        valid = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(
          e.target.value
        );
        if (valid) {
          setPhoneErrorMessage("");
        } else {
          setPhoneErrorMessage("Invalid phone number");
        }
        break;

      case "email":
        if (e.target.value.length === 0) {
          setEmailErrorMessage("");
          break;
        }
        valid = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(
          e.target.value
        );
        if (valid) {
          setEmailErrorMessage("");
        } else {
          setEmailErrorMessage("Invalid Email");
        }
        break;

      default:
        break;
    }
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const [isLoading, setIsLoading] = useState(false);
  const [msg, setMsg] = useState({
    msg: "",
    color: "",
  });

  useEffect(() => {
    if (msg.msg) {
      setTimeout(() => {
        setMsg({
          msg: "",
          color: "",
        });
      }, 3000);
    }
  }, [msg]);

  const sendEmail = async () => {
    setIsLoading(true);
    let res;
    try {
      if (!requestService) {
        res = await axios.post(
          "https://www.rseem.co/backend/api/contact-us",
          formData
        );
      } else {
        res = await axios.post(
          "https://www.rseem.co/backend/api/request-service",
          {
            ...formData,
            service_ids: services
              .filter((item) => item.selected)
              .map((item) => item.id),
          }
        );
      }
      if (res.status == 200) {
        setMsg({
          msg: "submitted successfully",
          color: "text-green-700",
        });
        setIsLoading(false);
      }
    } catch (e) {
      setMsg({
        msg: "something went wrong!!",
        color: "text-red-700",
      });
      setIsLoading(false);
    }
  };

  return (
    <>
      {requestService && (
        <div className="py-12" aria-label="choose services">
          <h2 className="textTitle text-center text-primary-100 my-12">
            {t("choose_service")}
          </h2>
          <div className="flex flex-wrap gap-x-8 sm:gap-x-16 gap-y-10 justify-center mb-12 mt-12 px-3 sm:px-[10%]">
            {services.map((service, i) => (
              <div
                key={i}
                className="shadow-md hover:shadow-lg transform transition duration-200 hover:scale-105 shrink-0  overflow-hidden relative cursor-pointer border"
                style={{ width: "150px", height: "100%", borderRadius: "10px" }}
                onClick={() => selectService(i)}
              >
                <div className="" />
                <Option {...service} />
              </div>
            ))}
          </div>
          {services.filter((service) => service.selected).length > 0 && (
            <div className="w-full flex items-center justify-center">
              <button
                onClick={() => {
                  router.push("/requestService/#contactForm");
                }}
                className="w-64 rounded-md shadow hover:shadow-md py-2 text-lg border-2 border-primary-100 text-primary-100 capitalize font-semibold"
              >
                {t("continue")}
              </button>
            </div>
          )}
        </div>
      )}
      <div
        id="contactForm"
        className="w-full  flex flex-col justify-center pt-20 pb-24 lg:py-24 bg-cover bg-right bg-no-repeat bg-services overflow-hidden"
        aria-label="contact us form"
      >
        <div className="px-[5%] sm:px-20 w-full  flex flex-col text-center justify-center align-center">
          {!requestService && (
            <h3
              ref={titleRef}
              className="font-normal text-4xl md:px-24 mb-6 text-white"
            >
              {t("contact")}
            </h3>
          )}
          <div className="md:px-24 text-white font-light mb-16" />
          <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mb-6 ">
            <div ref={nameRef} aria-label="text input">
              <BaseInput
                type="text"
                name="name"
                placeholder={t("name")}
                value={formData.name}
                onChange={(e: any) => {
                  handleChange(e);
                }}
              />
            </div>
            <div ref={subRef} aria-label="text input">
              <BaseInput
                type="text"
                name="subject"
                placeholder={t("subject")}
                value={formData.subject}
                onChange={(e: any) => {
                  handleChange(e);
                }}
              />
            </div>
            <div ref={emailRef} aria-label="email input">
              <BaseInput
                type="email"
                name="email"
                placeholder={t("email")}
                value={formData.email}
                onChange={(e: any) => {
                  handleChange(e);
                }}
                errorMessage={emailErrorMessage}
              />
            </div>
            <div ref={mobileRef}>
              <BaseInput
                type="number"
                name="phone"
                placeholder={t("phone")}
                value={formData.phone}
                onChange={(e: any) => {
                  handleChange(e);
                }}
                // errorMessage={phoneErrorMessage}
              />
            </div>
            <div className="md:col-span-2 " ref={messageRef}>
              <BaseTextarea
                name="message"
                placeholder={t("message")}
                value={formData.message}
                onChange={(e: any) => {
                  handleChange(e);
                }}
                rows="7"
              />
            </div>
          </div>
          <div className="flex flex-col items-center mt-6">
            <p className={`${msg.color}  textsm font-bold text-center`}>
              {msg.msg}
            </p>
            <button
              ref={buttonRef}
              className="text-white font-base text-2xl bg-primary-100 disabled:bg-primary-100/50 shadow-md w-full md:w-auto md:px-48 py-3 rounded-md  mx-auto  disabled:opacity-50 disabled:cursor-not-allowed"
              disabled={
                formData.name.length === 0 ||
                formData.subject.length === 0 ||
                // formData.phone.length === 0 ||
                formData.email.length === 0 ||
                formData.message.length === 0 ||
                // phoneErrorMessage.length !== 0 ||
                emailErrorMessage.length !== 0 ||
                (requestService &&
                  services.filter((item) => item.selected).length === 0)
              }
              onClick={() => sendEmail()}
            >
              {t("send")}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactUs;
