import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useFadeX, swiperAnimation } from "../utils/gsapAiamtiom/gsap";
import { responsiveLang } from "../utils/responsive";
import { BsChevronRight, BsChevronLeft } from "react-icons/bs";
const Dummy = ({ review }: any) => {
  return (
    <div className="h-full  flex flex-col items-center  ">
      <div className=" relative ">
        {/*  <div  className="absolute w-[160px] h-[160px] border border-primary-300 rounded-full -top-1 -right-3" /> */}
        <Image src={review.img} alt="avatar" width="160" height="160" />
      </div>
      <p className="texBody text-white leading-5 mt-4 w-[90%] max-w-[600px] ">
        {review.review}
      </p>
      <p className="font-myriad font-medium text-[#ddd] leading-5 mt-6">
        {review.company}
      </p>
    </div>
  );
};
const TwSwiper = () => {
  const { t, i18n } = useTranslation();
  const [curSlide, setCurSlide] = useState(0);
  // console.log(curSlide);
  const titleRef = useRef(null);
  const bodyRef = useRef(null);
  const swiperRef = useRef(null);
  useFadeX(titleRef, -40);
  useFadeX(bodyRef, 40);
  const animation = useRef<any>();
  useEffect(() => {
    animation.current = swiperAnimation(swiperRef);
    return () => animation.current.kill();
  }, []);

  const reviews = [
    {
      img: "/avatar.png",
      company: "شركة سوفتيك",
      review:
        "كم اسعدني التعامل مع شركة رسيم في تنفيذ المشاريع الخاصة بي , لديكم فريق رائع .",
    },
    {
      img: "/avatar.png",
      company: "شركة كيفا ",
      review: "تحليل وتنفيذ المشروع وفق أعلى متطلبات البنية التحتية ",
    },
    {
      img: "/avatar.png",
      company: "شركة ساحل الجمال للتجارة  ",
      review: "دقة في تسليم المشروع على أحدث التصاميم ",
    },
  ];

  useEffect(() => {
    let timeout = setTimeout(() => {
      if (curSlide === 2) {
        setCurSlide(0);
      } else {
        setCurSlide(curSlide + 1);
      }
    }, 3000);
    return () => clearTimeout(timeout);
  }, [curSlide]);

  return (
    <div
      className="relative min-h-[80vh] sm:min-h-[80vh] text-center px-2 sm:px-8"
      aria-label="Reviews"
    >
      <div
        className={`bg-swiper bg-cover bg-center absolute h-full  right-0 transition-transform duration-500 -z-[1]`}
        style={{
          width: `calc(100% + ${reviews.length} * 10vw )`,
          transform: `translateX(${10 * curSlide}vw)`,
        }}
      />
      <h3 ref={titleRef} className="font-cake textSubtitle text-white pt-16">
        {t("reviews")}
      </h3>
      <p
        ref={bodyRef}
        className="textBody text-lg text-white leading-5 mt-8 sm:w-1/2 mx-auto mb-32"
      >
        {t("reviews_info")}
      </p>
      <div className="relative">
        {curSlide > 0 && (
          <div
            className="z-10 absolute cursor-pointer left-0 top-16 sm:bottom-3 w-8 h-16 sm:w-[50px] sm:h-[100px]"
            onClick={() => curSlide > 0 && setCurSlide(curSlide - 1)}
          >
            <BsChevronLeft className="text-white font-thin" size={"6rem"} />
          </div>
        )}
        {curSlide < reviews.length - 1 && (
          <div
            className="z-10  absolute cursor-pointer  right-0 top-16 sm:bottom-3 w-8 h-16 sm:w-[50px] sm:h-[100px]"
            onClick={() => curSlide < 3 && setCurSlide(curSlide + 1)}
          >
            <BsChevronRight className="text-white font-thin" size={"6rem"} />
            {/* <Image src="/icons/chevronRight.png" alt="icon" layout="fill" /> */}
          </div>
        )}
        <div ref={swiperRef} className=" sm:px-[10%] h-auto ">
          <ul
            className="snap-x transition-all duration-700 flex"
            style={{
              transform: responsiveLang(
                i18n.language,
                `translate(${100 * curSlide}%)`,
                `translate(-${100 * curSlide}%)`
              ),
            }}
          >
            {reviews.map((item, i) => (
              <li
                key={6 * i}
                className={`snap-center w-full shrink-0 transition-opacity duration-700 ${
                  i === curSlide ? "opacity-100" : "opacity-0"
                }`}
              >
                <Dummy review={item} />
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};
export default TwSwiper;
