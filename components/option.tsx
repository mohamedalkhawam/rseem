import Image from "next/image";
import { useTranslation } from "react-i18next";

const Option = ({ title, icon, iconAlt, selected }: any) => {
  const { t } = useTranslation();
  return (
    <div
      aria-label="options"
      className={`flex flex-col items-center h-48 px-6 py-3 cursor-pointer  ${
        selected ? "movingBorder" : "white"
      }`}
    >
      <div className="mt-4">
        <Image src={icon} alt={title} width="80" height="80" />
      </div>
      <div>
        <p className="leading-5 select-none text-center text-gray-500 pt-3">
          {t(title)}
        </p>
      </div>
    </div>
  );
};

export default Option;
