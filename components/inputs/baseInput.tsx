import { forwardRef } from "react";

type propsType = {
  type: string;
  name: string;
  value: any;
  placeholder: string;
  onChange: any;
  errorMessage?: string;
  label?: string;
  className?: string;
};

const BaseInput = (
  {
    type,
    name,
    label,
    value,
    placeholder,
    onChange,
    errorMessage,
    className = "",
  }: propsType,
  ref: any
) => {
  return (
    <div className="w-full h-14 glass">
      <input
        className={`w-full h-full bg-transparent px-6 outline-none placeholder-base placeholder-white/70 text-white focus:rounded-lg focus:border focus:border-white/50 ${className}`}
        type={type}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        value={value}
      />
      <p className="text-red-700 text-left">{errorMessage}</p>
    </div>
  );
};

export default forwardRef(BaseInput);
