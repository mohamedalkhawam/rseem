type propsType = {
  name: string;
  value: any;
  placeholder: string;
  onChange: any;
  rows: any;
  errorMessage?: string;
};

const BaseTextarea = ({
  name,
  value,
  placeholder,
  onChange,
  errorMessage,
  rows,
}: propsType) => {
  return (
    <div className="w-full glass " style={{ height: "12rem" }}>
      <textarea
        className="w-full h-full bg-transparent px-6 py-5 outline-none placeholder-base placeholder-white/70 focus:rounded-lg focus:border focus:border-white/50 text-white"
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        value={value}
        rows={rows ? rows : "5"}
      />
      <p className="text-red-700 text-left">{errorMessage}</p>
    </div>
  );
};

export default BaseTextarea;
