import { t } from "i18next";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useRef } from "react";
import { useFooterColAnimation } from "../../utils/gsapAiamtiom/gsap";
import { BsTelephone } from "react-icons/bs";
import { MdOutlineMail, MdOutlineLocationOn } from "react-icons/md";
import { BsInstagram, BsFacebook, BsLinkedin } from "react-icons/bs";
import { AiFillTwitterCircle } from "react-icons/ai";
import { useRouter } from "next/router";

type companyProps = {
  name: string;
  link: string;
};

const Footer = () => {
  const logoRef = useRef(null);
  const text1Ref = useRef(null);
  const title2Ref = useRef(null);
  const text2Ref = useRef(null);
  const title3Ref = useRef(null);
  const text3Ref = useRef(null);
  const title4Ref = useRef(null);
  const text4Ref = useRef(null);
  useFooterColAnimation(logoRef, text1Ref);
  useFooterColAnimation(title2Ref, text2Ref);
  useFooterColAnimation(title3Ref, text3Ref);
  // useFooterColAnimation(title4Ref,text4Ref)

  const social = [
    {
      name: "insta",
      href: "tel:0544867202",
      icon: (
        <BsInstagram
          size={25}
          className="shrink-0 hover:text-blue-500 text-gray-100"
        />
      ),
    },
    {
      name: "facebook",
      href: "mailto:info@rseem.net",
      icon: (
        <BsFacebook
          size={25}
          className="shrink-0 hover:text-blue-500 text-gray-100"
        />
      ),
    },
    {
      name: "twitter",
      href: "https://twitter.com/Rseem_co",
      icon: (
        <AiFillTwitterCircle
          size={28}
          className="shrink-0 hover:text-[#188CD8] text-gray-100"
        />
      ),
    },
    {
      name: "linkedin",
      href: "المملكة العربية السعودية الرياض -واجهة الرياض",
      icon: (
        <BsLinkedin
          size={25}
          className="shrink-0 hover:text-[#0B65C2] text-gray-100"
        />
      ),
    },
  ];

  const company = [
    {
      name: t("contact"),
      link: "/contactUs",
    },
    {
      name: t("about_rseem"),
      link: "/aboutRseem",
    },
    {
      name: t("terms"),
      link: "/termsAndConditions",
    },
    {
      name: t("request-service"),
      link: "/requestService",
    },
  ];
  const contacts = [
    {
      name: "0544867202",
      href: "tel:0544867202",
      icon: <BsTelephone size={17} color="white" className="shrink-0" />,
    },
    {
      name: "info@rseem.net",
      href: "mailto:info@rseem.net",
      icon: <MdOutlineMail size={17} color="white" className="shrink-0" />,
    },
    {
      name: t("location_info"),
      href: "https://goo.gl/maps/zaP26mQQk7YZR1ZL9",
      icon: (
        <MdOutlineLocationOn size={20} color="white" className="shrink-0" />
      ),
    },
  ];
  const router = useRouter();
  return (
    <footer
      id="footer"
      className="bg-primary-300 px-6 md:px-[10%] pt-28 pb-8 "
      aria-label="footer"
    >
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-x-12 md:gap-x-16 gap-y-12">
        <div className="w-[14rem] mx-auto">
          <div
            ref={logoRef}
            onClick={() => {
              router.push("/");
            }}
            className="cursor-pointer"
          >
            <Image src="/logo.svg" alt="logo" width="200" height="75" />
          </div>
          <p
            ref={text1Ref}
            className="  text-gray-100 leading-5 text-sm md:text-base"
          >
            {t("company_info")}
          </p>
        </div>
        <div className="w-[14rem] sm:w-[8rem] mx-auto ">
          <h3
            ref={title2Ref}
            className=" font-semibold text-2xl text-primary-100 mb-5"
          >
            {t("company")}
          </h3>
          <div ref={text2Ref}>
            {company.map(({ name, link }: companyProps, i) => (
              <div key={name} className="my-1">
                <Link href={link}>
                  <a>
                    <p className="  text-gray-100 leading-5 text-sm md:text-base">
                      {name}
                    </p>
                  </a>
                </Link>
              </div>
            ))}
          </div>
        </div>
        <div className="w-[16rem]  mx-auto ">
          <h3
            ref={title3Ref}
            className=" font-semibold text-2xl text-primary-100 mb-5"
          >
            {t("contacts")}
          </h3>
          <div ref={text3Ref}>
            {contacts.map(({ name, href, icon }: any, i) => (
              <div key={name} className="my-1 flex items-center">
                {icon}
                <a
                  href={href}
                  className="mx-2"
                  target="_blank"
                  rel="noreferrer"
                >
                  <p className="  text-gray-100 leading-5 text-sm md:text-base">
                    {name}
                  </p>
                </a>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="flex justify-center mt-24 2xl:mt-12">
        {social.map(({ icon, href }: any, i) => (
          <div key={i} className="mx-2">
            <a href={href} target="_blank" rel="noreferrer">
              {icon}
              <span
                className="text-transparent absolute"
                style={{ zIndex: -10000 }}
              >
                {href}
              </span>
            </a>
          </div>
        ))}
      </div>
      <p className="  text-gray-100 text-center text-xs mt-5">
        Copyright © 2022 . All Rights Reserved
      </p>
    </footer>
  );
};
export default Footer;
