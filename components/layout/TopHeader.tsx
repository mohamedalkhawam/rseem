import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../../utils/responsive";
import MenuButton from "../menuButton";
import Image from "next/image";
import { tabsArrayProps } from "./Header";
import Tabs from "./Tabs";
import { MdArrowDropDown } from "react-icons/md";
import { MdOutlineLanguage } from "react-icons/md";
import {
  headerAnimation,
  topHeaderAnimation,
} from "../../utils/gsapAiamtiom/gsap";
import { useRouter } from "next/router";
type props = {
  homePage?: boolean;
  noAnimation?: boolean;
};

export default function TopHeader({ homePage, noAnimation }: props) {
  const [open, setOpen] = useState(false);
  const [atStart, setAtStart] = useState(true);
  const { i18n, t } = useTranslation();
  const headerRef = useRef(null);
  const headerLogoRef = useRef(null);
  const headerTabsRef = useRef(null);
  const menuButtonRef = useRef(null);
  const langRef = useRef(null);
  const animation = useRef<any>();

  const router = useRouter();

  useEffect(() => {
    if (noAnimation) {
    } else if (homePage) {
      animation.current = headerAnimation(
        headerRef,
        headerLogoRef,
        headerTabsRef,
        menuButtonRef,
        langRef,
        setOpen,
        setAtStart
      );
    } else {
      animation.current = topHeaderAnimation(
        headerRef,
        headerLogoRef,
        headerTabsRef,
        menuButtonRef,
        langRef,
        open,
        setAtStart
      );
    }

    return () => {
      if (!noAnimation) animation.current.kill();
    };
  }, []);

  useEffect(() => {
    const resizeFunc = () => {
      if (window.innerWidth > 768) {
        setOpen(false);
      }
    };
    const widthListener = addEventListener("resize", resizeFunc);

    return () => removeEventListener("resize", resizeFunc);
  }, []);

  const tabsArray: tabsArrayProps[] = [
    {
      name: t("home"),
      menuName: "home",
      action() {
        router.push("/");
      },
      tabsStyle: `${responsiveLang(
        i18n.language,
        "rounded-r-full",
        "rounded-l-full"
      )} w-[20%] `,
      leftPartInfo:
        "Lorem Ipsum Dolor Sit Amet,Consectetuer Adipiscing Elit, Sed Diam Nonummy Nibh Euismod Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit",
    },
    {
      name: t("services"),
      menuName: "services",
      tabsStyle: "w-[20%]",
      leftPartInfo: t("services_info"),
    },
    {
      name: t("techs"),
      menuName: "products",
      tabsStyle: "w-[20%]",
      leftPartInfo: t("tech_info"),
    },
    {
      name: t("portfolio"),
      menuName: "portfolio",
      action() {
        router.push("/#portfolio");
      },
      tabsStyle: "w-[20%]",
      leftPartInfo:
        "Lorem Ipsum Dolor Sit Amet,Consectetuer Adipiscing Elit, Sed Diam Nonummy Nibh Euismod Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit",
    },
    {
      name: t("company"),
      menuName: "company",
      tabsStyle: `${responsiveLang(
        i18n.language,
        "rounded-l-full",
        "rounded-r-full"
      )} w-[20%] `,
      leftPartInfo: t("company_info"),
      topHeader: true,
    },
  ];

  const drawerTabs = [
    {
      ...tabsArray[0],
    },
    {
      ...tabsArray[1],
      action: function () {
        router.push("/#services");
        setOpen(false);
      },
    },
    {
      ...tabsArray[2],
      action: function () {
        router.push("/#techs");
        setOpen(false);
      },
    },
    {
      ...tabsArray[3],
      action: function () {
        router.push("/#portfolio");
        setOpen(false);
      },
    },
    {
      ...tabsArray[4],
      action: function () {
        router.push(`${window.location.pathname}#footer`);
        setOpen(false);
      },
    },
    {
      name: t("request-service"),
      menuName: "request",
      tabsStyle: ` w-[20%] `,
      leftPartInfo: t("company_info"),
      topHeader: true,
    },
  ];
  return (
    <nav
      ref={headerRef}
      className={`flex ${responsiveLang(
        i18n.language,
        "flex-row-reverse",
        ""
      )} items-center w-full ${
        noAnimation ? "absolute left-0 top-0" : "fixed bg-white"
      }  left-0 top-0  px-3 md:px-6 py-2 md:py-0 lg:px-20 ${
        atStart ? "" : "shadow-lg"
      }  z-50 `}
      aria-label="navbar"
    >
      <div
        ref={headerLogoRef}
        onClick={() => {
          router.push("/");
        }}
        className=" w-[140px] h-[50px] lg:w-[160px] lg:h-[60px] shrink-0 relative z-50 cursor-pointer"
      >
        <Image
          src={
            atStart && !open
              ? "/logo.svg"
              : atStart && open
              ? "/logo2.svg"
              : "/logo2.svg"
          }
          alt="logo"
          width="200"
          height="75"
        />
      </div>
      <ul ref={headerTabsRef} className=" ml-auto hidden md:flex">
        {tabsArray.map(({ name, menuName, leftPartInfo, action }, i) => (
          <li onClick={action} key={name}>
            <Tabs
              name={name}
              menuName={menuName}
              leftPartInfo={leftPartInfo}
              menuBottom={true}
            />
          </li>
        ))}
      </ul>
      <div
        ref={langRef}
        dir="ltr"
        className="ml-auto flex flex-row items-center group relative z-[200] md:ml-3  uppercase text-lg "
      >
        <h3
          className={`select-none mr-1 text-base ${
            atStart && !open
              ? "text-white"
              : atStart && open
              ? "text-[$444]"
              : "text-[$444]"
          } transition-colors duration-500`}
        >
          {i18n.language === "ar" ? "العربية" : "English"}
        </h3>
        <MdOutlineLanguage
          className={`  rounded-full text-lg gg ${
            atStart && !open
              ? "text-white"
              : atStart && open
              ? "text-[$444]"
              : "text-[$444]"
          } transition-colors duration-500`}
        />
        <MdArrowDropDown
          className={` gg ${responsiveLang(
            i18n.language,
            "-right-2 top-7",
            "-right-1 top-5"
          )} ${
            atStart && !open
              ? "text-white"
              : atStart && open
              ? "text-[$444]"
              : "text-[$444]"
          } transition-colors duration-500`}
        />
        <div
          className={`flex flex-col items-center justify-center rounded-lg top-[110%] ${responsiveLang(
            i18n.language,
            "-left-3",
            "-left-1"
          )}  select-none scale-y-0 group-hover:scale-y-100 transform transition-all duration-500 origin-top absolute bg-white`}
        >
          <p
            className="cursor-pointer hover:bg-gray-200  w-full text-center px-4 py-2 border rounded-t-lg"
            onClick={() => i18n.changeLanguage("en")}
          >
            English
          </p>
          <p
            className="cursor-pointer hover:bg-gray-200 w-full text-center px-4 py-2 border rounded-b-lg"
            onClick={() => i18n.changeLanguage("ar")}
          >
            العربية
          </p>
        </div>
      </div>
      <div
        ref={menuButtonRef}
        className=" h-full cursor-pointer md:hidden relative z-100 "
      >
        <MenuButton className="" active={open} onClick={() => setOpen(!open)} />
        {/*  <BiSupport
            size={56}
            className="  bg-primary-100 animate-bounce group-hover:animate-none cursor-pointer shadow-blue-100 shadow-sm text-white rounded-full p-3"
          /> */}
      </div>
      <div
        className={`w-full  bg-white  rounded-b-3xl absolute z-1 top-0 left-0  origin-top transition-transform duration-500 transform ${
          open ? "scale-y-100 shadow-md" : "scale-y-[0.2] -top-20"
        }`}
      >
        {open && (
          <ul
            ref={headerTabsRef}
            className="flex flex-col w-full h-full md:hidden mt-16"
          >
            {drawerTabs.map(
              ({ name, menuName, leftPartInfo, action, topHeader }, i) => (
                <li onClick={action} key={name}>
                  <Tabs
                    name={name}
                    menuName={menuName}
                    leftPartInfo={leftPartInfo}
                    menuBottom={true}
                    homePage={homePage}
                    topHeader={topHeader}
                  />
                </li>
              )
            )}
          </ul>
        )}
      </div>
    </nav>
  );
}
