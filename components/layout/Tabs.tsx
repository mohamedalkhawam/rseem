import { t } from "i18next";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import { techArr, techObj } from "../../utils/consts";
import { responsiveLang } from "../../utils/responsive";
type tabsProps = {
  name: string;
  menuName: string;
  tabsStyle?: string;
  leftPartInfo: string;
  menuBottom?: boolean;
  homePage?: boolean;
  topHeader?: boolean;
  action?: any;
};

type leftPart = {
  name: string;
  info: string;
  menuName: string;
};

const LeftPart = ({ name, info, menuName }: leftPart) => {
  const router = useRouter();
  const { i18n } = useTranslation();
  return (
    <div className="relative  ">
      <div className="flex items-center ">
        <div
          className={`w-[1.4rem] h-[1.4rem]  motion-safe:animate-pulse   bg-primary-100 rounded-full ${responsiveLang(
            i18n.language,
            "ml-4",
            "mr-1"
          )}  mt-1 `}
        />
        <h2 className="textTitle first-letter:text-4xl text-primary-200 pt-1  capitalize transition-all duration-700 transform opacity-0 group-hover:opacity-100 -translate-y-[5rem] group-hover:translate-y-0">
          {name}
        </h2>
      </div>
      <p
        className={`${responsiveLang(
          i18n.language,
          "pr-7 text-right text-sm ",
          " pl-7 text-left text-xs lg:pr-3"
        )}  text-[#777]  font-light leading-4 mt-1 transition-all duration-700 delay-700 opacity-0 group-hover:opacity-100`}
      >
        {info}
      </p>
      {menuName === "services" && (
        <div
          onClick={() => router.push("/requestService")}
          className={`mx-auto ${responsiveLang(
            i18n.language,
            "",
            " "
          )} mt-5 relative bg-gradient-to-r from-primary-300 to-primary-100 w-40 -z-1 text-center py-2 rounded-lg cursor-pointer hover:drop-shadow-lg `}
        >
          <div className="bg-white absolute w-[9.75rem] h-10 rounded-md top-[0.125rem] left-[0.125rem]" />
          <div className="bg-gradient-to-r from-primary-300 to-primary-100 bg-clip-text text-transparent relative z-1 text-lg font-semibold select-none ">
            {t("request-service")}
          </div>
        </div>
      )}
    </div>
  );
};

const homeObj: {
  [x: string]: string[];
} = {
  frontEnd: ["next", "React", "Vue", "angular", "Tailwindcss"],
  backEnd: ["Node", "Laravel", "golang", "java", "Python"],
  mobile: ["react-native", "flutter", "swift", "kotlen", "xamarin"],
};
const Services = () => {
  const { i18n, t } = useTranslation();
  return (
    <div className="grid grid-cols-3 justify-center text-center overflow-hidden h-full">
      <div
        className={`delay-200 transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <div className="flex flex-col justify-evenly h-full">
          <div>
            <Link href="/services/websites">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/websites.svg"
                    alt={t("websites")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("websites")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/mobile-applications">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/mobile-applications.svg"
                    alt={t("mobile-applications")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("mobile-applications")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/e-commerce">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/e-commerce.svg"
                    alt={t("e-commerce")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("e-commerce")}
                </p>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div
        className={`delay-[400ms] transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <div className="flex flex-col justify-evenly h-full ">
          <div>
            <Link href="/services/custome-softwares">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/custome-softwares.svg"
                    alt={t("custome-softwares")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("custome-softwares")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/graphic-design">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/graphic-design.svg"
                    alt={t("graphic-design")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("graphic-design")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/graphic-design">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/artifical-intelligence.svg"
                    alt={t("artifical-intelligence")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("artifical-intelligence")}
                </p>
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div
        className={`delay-[600ms] transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <div className="flex flex-col justify-evenly h-full">
          <div>
            <Link href="/services/virtual-reality">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/digital-marketing.svg"
                    alt={t("digital-marketing")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("digital-marketing")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/hosting">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/hosting.svg"
                    alt={t("hosting")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("hosting")}
                </p>
              </a>
            </Link>
          </div>
          <div>
            <Link href="/services/startups">
              <a className="cursor-pointer">
                <div>
                  <Image
                    src="/startups.svg"
                    alt={t("startups")}
                    height="25"
                    width="25"
                  />
                </div>
                <p className="text-[#666] font-medium capitalize">
                  {t("startups")}
                </p>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};
const Techs = () => {
  const { i18n } = useTranslation();
  return (
    <div
      className="grid grid-cols-3 justify-center text-center overflow-hidden py-5"
      dir="ltr"
      aria-label="tabs"
    >
      <div
        className={`delay-200 transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <h3 className="font-semibold text-[#444] first-letter:text-primary-100 ">
          FRONT-END
        </h3>
        <div className="mt-2">
          {techObj["FRONT-END"].map((ite: any) => (
            <div key={ite.name} className="flex pl-12 my-1">
              {ite.icon && (
                <Image src={ite.icon} alt={ite.icon} width={20} height={20} />
              )}
              <p className="text-center text-gray-600 ml-3 text-sm">
                {ite.name}
              </p>
            </div>
          ))}
        </div>
      </div>
      <div
        className={`delay-[400ms] transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <h3 className="font-semibold text-[#444] first-letter:text-primary-100 ">
          BACK-END
        </h3>
        <div className="mt-2">
          {techObj["BACK-END"].map((ite: any) => (
            <div key={ite.name} className="flex pl-12 my-1">
              {ite.icon && (
                <Image src={ite.icon} alt={ite.icon} width={20} height={20} />
              )}
              <p className="text-center text-gray-600 ml-3 text-sm">
                {ite.name}
              </p>
            </div>
          ))}
        </div>
      </div>
      <div
        className={`delay-[600ms] transition-all duration-700 transform ${responsiveLang(
          i18n.language,
          "-translate-x-[44rem]",
          "translate-x-[44rem]"
        )}  group-hover:translate-x-0 easeOutBack`}
      >
        <h3 className="font-semibold text-[#444] first-letter:text-primary-100 ">
          MOBILE
        </h3>
        <div className="mt-2">
          {techObj["MOBILE"].map((ite: any) => (
            <div key={ite.name} className="flex pl-12 my-1">
              {ite.icon && (
                <Image src={ite.icon} alt={ite.icon} width={20} height={20} />
              )}
              <p className="text-center text-gray-600 ml-3 text-sm">
                {ite.name}
              </p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
const Company = () => {
  const { i18n } = useTranslation();
  const router = useRouter();
  return (
    <div className="flex flex-col gap-x-3 justify-center text-center overflow-hidden py-14 items-center opacity-0 group-hover:opacity-100 transition-all duration-[1.5s]">
      <div>
        <Link href="/contactUs">
          <a>
            <p className="text-primary-200 text-lg font-semibold capitalize hover:underline ">
              {t("contact")}
            </p>
          </a>
        </Link>
      </div>
      <div>
        <Link href="/aboutRseem">
          <a>
            <p className="text-primary-200 text-lg font-semibold capitalize hover:underline">
              {t("about_rseem")}
            </p>
          </a>
        </Link>
      </div>
      <div>
        <Link href="/termsAndConditions">
          <a>
            <p className="text-primary-200 text-lg font-semibold capitalize hover:underline">
              {t("terms")}
            </p>
          </a>
        </Link>
      </div>
      <div
        onClick={() => router.push("/requestService")}
        className={`col-span-3 mt-5 mx-auto  relative bg-gradient-to-r from-primary-300 to-primary-100 w-40 -z-1 text-center py-2 rounded-lg cursor-pointer hover:drop-shadow-lg `}
      >
        <div className="bg-white absolute w-[9.75rem] h-10 rounded-md top-[0.125rem] left-[0.125rem]" />
        <div className="bg-gradient-to-r from-primary-300 to-primary-100 bg-clip-text text-transparent relative z-1 text-lg font-semibold select-none ">
          {t("request-service")}
        </div>
      </div>
    </div>
  );
};

const Tabs = ({
  name,
  menuName,
  tabsStyle,
  leftPartInfo,
  menuBottom,
  topHeader,
  action,
}: tabsProps) => {
  const { i18n } = useTranslation();
  const rendermenu: any = {
    services: <Services />,
    products: <Techs />,
    company: <Company />,
  };

  return (
    <div aria-haspopup="true" className={`group w-full `} onClick={action}>
      <div
        className={` ${
          tabsStyle ? tabsStyle : "px-3 lg:px-5 md:py-8 "
        } md:flex md:justify-center cursor-pointer overflow-hidden py-4 transition-all h-full  duration-500 text-center hover:bg-primary-100/40   ${
          menuName === "request" && topHeader ? "rounded-b-3xl md:rounded" : ""
        }`}
      >
        <div className="uppercase h-full  group flex justify-center ">
          {responsiveLang(
            i18n.language,
            <p className=" group-hover:animate-pulse">{name}</p>,
            name.split("").map((l, i) => (
              <p
                key={l + i}
                className="group-hover:animate-bounce font-medium"
                style={{ animationDelay: `${i * 70}ms` }}
              >
                {l}
              </p>
            ))
          )}
        </div>
      </div>

      {menuName !== "home" && menuName !== "portfolio" && (
        <div
          className={`w-[50rem] py-1 fixed h-72 mb-4 bottom-[100%] md:group-hover:h-72 -top-[1000rem] ${
            menuBottom
              ? "md:group-hover:top-[5.5rem] md:right-10 lg:right-20"
              : `md:group-hover:-top-[18rem] ${responsiveLang(
                  i18n.language,
                  "right-0",
                  "left-0"
                )}`
          } md:group-hover:absolute group-hover:block z-50`}
        >
          <div className="bg-white w-[700px]  lg:w-full h-full flex rounded-[25px] shadow-md">
            <div className="w-1/3  h-full rounded-l-[25px] pl-8 pr-2 py-12 ">
              <LeftPart name={name} info={leftPartInfo} menuName={menuName} />
            </div>
            <div className="w-2/3 bg-gray-100 h-full rounded-[25px] py-4 px-3 ">
              {rendermenu[menuName]}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default Tabs;
