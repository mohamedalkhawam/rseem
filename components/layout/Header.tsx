import React, { useEffect, useRef, useState } from "react";
import Tabs from "./Tabs";
import { BiSupport } from "react-icons/bi";
import { MdArrowDropDown } from "react-icons/md";
import useScrolledTop from "../../hooks/useScrolledTop";
import useWindowSize from "../../hooks/useWindowSize";
import Image from "next/image";
import { headerAnimation } from "../../utils/gsapAiamtiom/gsap";
import { useTranslation } from "react-i18next";
import { responsiveLang } from "../../utils/responsive";
import MenuButton from "../menuButton";
import TopHeader from "./TopHeader";
import { useRouter } from "next/router";

export type tabsArrayProps = {
  name: string;
  menuName: string;
  tabsStyle?: string;
  leftPartInfo: string;
  action?: any;
  topHeader?: boolean;
};

type headerType = {
  open: boolean;
  setOpen: any;
  passRef1?: any;
  passRef2?: any;
};

const Header = ({ open, setOpen, passRef1, passRef2 }: headerType) => {
  const { t, i18n } = useTranslation();
  const router = useRouter();

  const tabsArray: tabsArrayProps[] = [
    {
      name: t("home"),
      menuName: "home",
      action() {
        router.push("/");
      },
      tabsStyle: `${responsiveLang(
        i18n.language,
        "rounded-r-full",
        "rounded-l-full"
      )} w-full `,
      leftPartInfo:
        "Lorem Ipsum Dolor Sit Amet,Consectetuer Adipiscing Elit, Sed Diam Nonummy Nibh Euismod Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit",
    },
    {
      name: t("services"),
      menuName: "services",
      tabsStyle: "w-full",
      leftPartInfo: t("services_info"),
    },
    {
      name: t("techs"),
      menuName: "products",
      tabsStyle: "w-full",
      leftPartInfo: t("tech_info"),
    },
    {
      name: t("portfolio"),
      menuName: "portfolio",
      action: function () {
        router.push("/#portfolio");
      },
      tabsStyle: "w-full",
      leftPartInfo:
        "Lorem Ipsum Dolor Sit Amet,Consectetuer Adipiscing Elit, Sed Diam Nonummy Nibh Euismod Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit",
    },
    {
      name: t("company"),
      menuName: "company",
      tabsStyle: `${responsiveLang(
        i18n.language,
        "rounded-l-full",
        "rounded-r-full"
      )} w-full `,
      leftPartInfo: t("company_info"),
    },
  ];

  return (
    <header className="relative z-50" aria-label="header">
      <nav
        ref={passRef1}
        className="relative hidden md:flex  rounded-full w-[44rem] lg:w-[50rem]   items-center bg-white mx-auto"
      >
        <div
          ref={passRef2}
          className={`absolute group hidden lg:flex items-center justify-center ${responsiveLang(
            i18n.language,
            "-right-20",
            "-left-20"
          )} w-[56px] h-[56px]  cursor-pointer `}
        >
          <div className="transition-all duration-700 bg-primary-100 group-hover:bg-white rounded-full p-2">
            {
              <BiSupport className="w-[40px] h-[40px] transform transition-all duration-700 text-white group-hover:text-primary-100 rotate-[360deg] group-hover:rotate-0" />
            }
          </div>
        </div>
        {tabsArray.map(
          (
            { name, menuName, tabsStyle, leftPartInfo, action = () => {} },
            i
          ) => (
            <Tabs
              key={name + i}
              name={name}
              menuName={menuName}
              tabsStyle={tabsStyle}
              leftPartInfo={leftPartInfo}
              action={action}
            />
          )
        )}
      </nav>
      <TopHeader homePage />
    </header>
  );
};

export default Header;
