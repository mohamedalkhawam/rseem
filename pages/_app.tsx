import "../styles/globals.css";
import type { AppProps } from "next/app";
import "../locales/i18n";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import Script from "next/script";
import { responsiveLang } from "../utils/responsive";
function MyApp({ Component, pageProps }: AppProps) {
  const { i18n } = useTranslation();

  useEffect(() => {
    if (i18n.language === "ar") {
      document.documentElement.lang = "ar";
    } else {
      document.documentElement.lang = "en";
    }
  }, [i18n.language]);

  return (
    <>
      <noscript>
        <iframe
          src="https://www.googletagmanager.com/ns.html?id=GTM-WWPGLG8"
          height="0"
          width="0"
          style={{ display: "none", visibility: "hidden" }}
        ></iframe>
      </noscript>
      <Script
        strategy="lazyOnload"
        src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
      />
      <Script strategy="lazyOnload" id="googleAnalytics">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}');
    `}
      </Script>
      <Script strategy="lazyOnload" id="googleTagManager">
        {`(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WWPGLG8');`}
      </Script>
      <div
        className={` mx-auto overflow-hidden   ${responsiveLang(
          i18n.language,
          "Almarai",
          "calibri"
        )}`}
      >
        <Component {...pageProps} />
      </div>
    </>
  );
}

export default MyApp;
