import Cors from "cors";
import { getAllPostSlugs } from "../../utils/posts";
const { SitemapStream, streamToPromise } = require("sitemap");
const { Readable } = require("stream");
// Initializing the cors middleware
const cors = Cors({
  methods: ["*"],
});
function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }
      return resolve(result);
    });
  });
}
async function handler(req, res) {
  // Run the middleware
  await runMiddleware(req, res, cors);
  //   try {
  //     // An array with your links
  //     const links = [];
  //     getAllPostSlugs().map((post) => {
  //       links.push({
  //         url: `/blog/${post.params.slug}`,
  //         changefreq: "daily",
  //         priority: 0.9,
  //       });
  //     });

  //     // Add other pages
  //     const pages = [
  //       "/",
  //       "/services",
  //       "/services/websites",
  //       "/services/mobile-applications",
  //       "/services/custome-softwares",
  //       "/services/e-commerce",
  //       "/services/hosting",
  //       "/services/startups",
  //       "/services/graphic-design",
  //       "/services/digital-marketing",
  //       "/services/artifical-intelligence",
  //       "/services/virtual-reality",
  //       "/aboutRseem",
  //       "/contactUs",
  //       "/requestService",
  //       "/termsAndConditions",
  //       "/contactUs",
  //     ];
  //     pages.map((url) => {
  //       links.push({
  //         url,
  //         changefreq: "daily",
  //         priority: 0.9,
  //       });
  //     });

  //     // Create a stream to write to
  //     const stream = new SitemapStream({
  //       hostname: `https://${req.headers.host}`,
  //     });

  //     res.writeHead(200, {
  //       "Content-Type": "application/xml",
  //     });

  //     const xmlString = await streamToPromise(
  //       Readable.from(links).pipe(stream)
  //     ).then((data) => data.toString());

  //     res.end(xmlString);
  //   } catch (e) {
  //     console.log(e);
  //     res.send(JSON.stringify(e));
  //   }
  // Create a stream to write to
  const links = [
    "/",
    "/services",
    "/services/websites",
    "/services/mobile-applications",
    "/services/custome-softwares",
    "/services/e-commerce",
    "/services/hosting",
    "/services/startups",
    "/services/graphic-design",
    "/services/digital-marketing",
    "/services/artifical-intelligence",
    "/services/virtual-reality",
    "/aboutRseem",
    "/contactUs",
    "/requestService",
    "/termsAndConditions",
    "/contactUs",
  ].map((item) => ({ url: item, changefreq: "daily", priority: 0.3 }));
  const stream = new SitemapStream({ hostname: `https://${req.headers.host}` });
  res.writeHead(200, {
    "Content-Type": "application/xml",
  });

  const xmlString = await streamToPromise(
    Readable.from(links).pipe(stream)
  ).then((data) => data.toString());

  res.end(xmlString);
}

export default handler;
