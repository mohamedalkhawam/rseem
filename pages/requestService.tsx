import Head from "next/head";
import { useTranslation } from "react-i18next";
import ContactUs from "../components/contactUs";
import Footer from "../components/layout/Footer";
import TopHeader from "../components/layout/TopHeader";
import SubscribeBox from "../components/SubscribeBox";
import { responsiveLang } from "../utils/responsive";
import { useState } from "react";
import { t } from "i18next";

export default function RequestService() {
  const { i18n } = useTranslation();

  return (
    <div className="" dir={responsiveLang(i18n.language, "rtl", "ltr")}>
      <Head>
        <title>Request a service</title>
        <meta name="title" content="request a service" />
        <meta
          name="keywords"
          content="RSEEM, rseem, rseem, رسيم, software, house, service, website, html, HTML, css, GoLang, PHP, CSS, JavaScript, C++, .net, saudi arabia, Saudi Arabia, KSA, رسيم الرياض , Rseem, rseem, rseem.net, rseem.com, reseem.co, rseem.org, websites, mobile, laptop, watch, apple, watch, applications, application, mobile application, web application, system, design, UI/UX, ui/ux, AI, ai, about, rseem  "
        />
        <meta name="description" content={t("company_info")} />
        <link rel="icon" href="/rLogo2.png" />
      </Head>
      <TopHeader homePage={false} />
      <div className="bg-topHeader bg-cover h-[35vh] w-full  ">
        <div className="w-full h-full bg-black bg-opacity-[35%] flex items-end justify-center sm:justify-start">
          <h2 className="textSubtitle text-white sm:mx-20 mb-12">
            {t("request-service")}
          </h2>
        </div>
      </div>

      <section className="">
        <ContactUs requestService />
      </section>
      <section>
        <div className="px-[10%] relative top-16">
          <SubscribeBox />
        </div>
      </section>
      <section>
        <Footer />
      </section>
    </div>
  );
}
