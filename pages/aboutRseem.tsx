import Head from "next/head";
import { useTranslation } from "react-i18next";
import ContactUs from "../components/contactUs";
import Footer from "../components/layout/Footer";
import TopHeader from "../components/layout/TopHeader";
import SubscribeBox from "../components/SubscribeBox";
import { responsiveLang } from "../utils/responsive";
import { BsTelephone } from "react-icons/bs";
import { MdOutlineMail, MdOutlineLocationOn } from "react-icons/md";
import { t } from "i18next";

export default function AboutRseem() {
  const { i18n } = useTranslation();
  const contacts = [
    {
      name: "0544867202",
      href: "tel:0544867202",
      icon: <BsTelephone size={17} className="shrink-0 text-primary-100" />,
    },
    {
      name: "info@rseem.net",
      href: "mailto:info@rseem.net",
      icon: <MdOutlineMail size={17} className="shrink-0 text-primary-100" />,
    },
    {
      name: "المملكة العربية السعودية الرياض -واجهة الرياض",
      icon: (
        <MdOutlineLocationOn size={20} className="shrink-0 text-primary-100" />
      ),
    },
  ];
  return (
    <div className="" dir={responsiveLang(i18n.language, "rtl", "ltr")}>
      <Head>
        <title>{t("about_rseem")}</title>
        <meta
          name="keywords"
          content="RSEEM, rseem, rseem, رسيم, software, house, service, website, html, HTML, css, GoLang, PHP, CSS, JavaScript, C++, .net, saudi arabia, Saudi Arabia, KSA, رسيم الرياض , Rseem, rseem, rseem.net, rseem.com, reseem.co, rseem.org, websites, mobile, laptop, watch, apple, watch, applications, application, mobile application, web application, system, design, UI/UX, ui/ux, AI, ai, about, rseem  "
        />
        <meta name="description" content={t("company_info")} />
        <link rel="icon" href="/rLogo2.png" />
      </Head>
      <TopHeader homePage={false} />
      <div className="bg-topHeader bg-cover h-[35vh] w-full flex items-end justify-center sm:justify-start">
        <div className="w-full h-full bg-black bg-opacity-[35%] flex items-end justify-center sm:justify-start">
          <h2 className="textSubtitle text-white sm:mx-20 mb-12">
            {t("about_rseem")}
          </h2>
        </div>
      </div>
      <div className="md:grid md:grid-cols-2 justify-center py-12 px-[5%] sm:px-20">
        <div className="w-[14rem] mb-8 md:mb-0 ">
          <h3 className="textSubtitle text-primary-100 mb-6">{t("rseem")}</h3>
          <ul>
            {contacts.map(({ name, href, icon }: any, i) => (
              <li key={name} className="my-1 textBody flex items-center">
                {icon}
                <a href={href} className="mx-2">
                  <p className=" textBody text-gray-500 leading-5 text-sm md:text-base">
                    {name}
                  </p>
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div className="w-full" aria-label="location Maps googlemaps ">
          <iframe
            width="100%"
            height="400"
            frameBorder="0"
            scrolling="no"
            marginHeight={0}
            marginWidth={0}
            src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
          >
            <a href="https://www.gps.ie/golf-gps/">golf gps watch</a>
          </iframe>
        </div>
      </div>
      <section>
        <div className="px-[10%] relative top-16">
          <SubscribeBox />
        </div>
      </section>
      <section>
        <Footer />
      </section>
    </div>
  );
}
