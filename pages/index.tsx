import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import Header from "../components/layout/Header";
import ProductsTabs from "../components/ProductsTabs";
import SubscribeBox from "../components/SubscribeBox";
import Footer from "../components/layout/Footer";
import Technologies from "../components/Technologies";
import FeaturedServices from "../components/FeaturedServices";
import TopCustomers from "../components/TopCustomers";
import { homeAnimate } from "../utils/gsapAiamtiom/gsap";
import { responsiveLang } from "../utils/responsive";
import { useRouter } from "next/router";
import TopHeader from "../components/layout/TopHeader";
const Home: NextPage = () => {
  const [open, setOpen] = useState(false);
  const logoRef = useRef(null);
  const titleRef = useRef(null);
  const titleCircleRef = useRef(null);
  const bodyRef = useRef(null);
  const buttonRef = useRef(null);
  const buttonCircleRef = useRef(null);
  const navRef = useRef(null);
  const navCircleRef = useRef(null);
  const { t, i18n } = useTranslation();
  const router = useRouter();

  const animation = useRef<any>();
  useEffect(() => {
    animation.current = homeAnimate(i18n.language, [
      logoRef,
      titleCircleRef,
      titleRef,
      bodyRef,
      buttonRef,
      buttonCircleRef,
      navRef,
      navCircleRef,
    ]);
    return () => {
      animation.current.kill();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  //
  return (
    <main
      lang={i18n.language}
      className={` mx-auto overflow-hidden`}
      dir={responsiveLang(i18n.language, "rtl", "ltr")}
    >
      <Head>
        <title>Home</title>
        <meta
          name="keywords"
          content="RSEEM, rseem, rseem, رسيم, software, house, service, website, html, HTML, css, GoLang, PHP, CSS, JavaScript, C++, .net, saudi arabia, Saudi Arabia, KSA, رسيم الرياض , Rseem, rseem, rseem.net, rseem.com, reseem.co, rseem.org, websites, mobile, laptop, watch, apple, watch, applications, application, mobile application, web application, system, design, UI/UX, ui/ux, AI, ai, about, rseem  "
        />
        <meta name="description" content={t("company_info")} />
        <link rel="icon" href="/rLogo2.png" />
      </Head>
      <section aria-label="first-screen header slider parth">
        <div className="h-screen w-full bg-home-header  bg-cover bg-center  ">
          <div className="w-full h-full bg-black bg-opacity-[35%] flex flex-col justify-between">
            <div className="relative w-full md:hidden">
              <TopHeader noAnimation={true} />
            </div>
            <div className="flex items-center  absolute right-1 top-4 sm:right-3 "></div>
            <div
              ref={logoRef}
              className="mx-auto  justify-center pt-14 hidden md:flex"
            >
              <Image src="/logo.svg" alt="logo" width="225" height="84" />
            </div>
            <div
              className={`mx-auto lg:mx-0 w-[90%] sm:w-[60%] lg:w-[45%] ${responsiveLang(
                i18n.language,
                "lg:pr-[10%]",
                "lg:pl-[10%]"
              )}  mt-[5%] mb-[10%] `}
            >
              <div>
                <div className=" relative mb-6">
                  <h2
                    ref={titleRef}
                    className={` 	  font-normal md:font-bold text-3xl   text-white relative z-[1] tracking-wider  ${responsiveLang(
                      i18n.language,
                      " ",
                      "first-letter:text-[3.2rem] first-letter:mr-3 px-2 "
                    )}`}
                  >
                    {t("keepOnInspiring")}
                  </h2>
                  <div
                    ref={titleCircleRef}
                    className={`absolute w-[3.25rem] h-[3.25rem] md:w-[3.4rem] md:h-[3.4rem] bg-primary-100 rounded-full -top-2 ${responsiveLang(
                      i18n.language,
                      "hidden",
                      "-left-[0.4rem] md:-left-[0.29rem]"
                    )}  `}
                  />
                </div>
                <p
                  ref={bodyRef}
                  className={` textBody font-normal text-white ${responsiveLang(
                    i18n.language,
                    "pr-12",
                    "pl-12"
                  )} `}
                >
                  {t("inspiring_info")}
                </p>
              </div>
              <div
                className={`group  flex justify-end mt-8 ${responsiveLang(
                  i18n.language,
                  "mr-auto",
                  "ml-auto"
                )}  w-[14rem] `}
              >
                <div
                  ref={buttonCircleRef}
                  className={`relative flex group justify-center w-10 h-10 bg-primary-100 rounded-full p-1 z-[10] group-hover:bg-transparent  group-hover:border-2 group-hover:border-white ${responsiveLang(
                    i18n.language,
                    "ml-2",
                    "mr-2"
                  )} transition-[margin] duration-300 `}
                >
                  <Image
                    src="/rLogo2White.png"
                    alt="R"
                    width="32"
                    height="32"
                    className="transitition-all duration-500 opacity-0 group-hover:opacity-100 transform group-hover:rotate-[360deg] "
                  />
                </div>
                <div
                  className={`h-[32px] transition-transform duration-300 ${responsiveLang(
                    i18n.language,
                    "group-hover:-translate-x-4",
                    "group-hover:translate-x-4"
                  )}`}
                >
                  <button
                    ref={buttonRef}
                    className="bg-white px-6  rounded-full relative text-base  z-[11] h-full pb-1 mt-1 "
                    onClick={() =>
                      setTimeout(() => {
                        router.push("/#techs2");
                      }, 1000)
                    }
                  >
                    {t("find")}
                  </button>
                </div>
              </div>
            </div>
            <div className="mb-[10%]">
              <Header
                passRef1={navRef}
                passRef2={navCircleRef}
                open={open}
                setOpen={setOpen}
              />
            </div>
          </div>
        </div>
      </section>
      <section aria-label="Rseem Technologies stack">
        <div id="techs2" className="mb-52 relative">
          <div id="techs" className="relative -top-32" />
          <Technologies />
        </div>
      </section>
      <section aria-label="Rseem Featured Services">
        <div className="relative">
          <div id="services" className="relative -top-52" />
          <FeaturedServices />
        </div>
      </section>
      <section aria-label="Rseem Featured Products">
        <div className=" px-[10%] relative">
          <div id="portfolio" className="relative -top-32" />
          <ProductsTabs />
        </div>
      </section>
      <section aria-label="Rseem Top Customer">
        <TopCustomers />
      </section>
      {/* <section>
        <TwSwiper />
      </section> */}
      <section aria-label=" Subscribe to newsletter">
        <div className="px-[10%] relative top-16">
          <SubscribeBox />
        </div>
      </section>
      <section aria-label="Footer Section">
        <Footer />
      </section>
    </main>
  );
};

export default Home;
